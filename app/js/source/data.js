var data = {

	/* MAIN NAVIGATION--------
	------------------------*/
	main: [
		{
			mainMenu: [
				{
					caption: "Види туристичних ресурсів",
					icon: "mdi-beach",
					content: [
						{
							caption: "Природно-кліматичні",
							icon: "mdi-pine-tree",
							route: "#content/naturaly-climatic"
						},

						{
							caption: "Природно-антропогенні",
							icon: "mdi-factory",
							route: "#content/naturaly-anthropogenic"
						},

						{
							caption: "Історико-культурні",
							icon: "mdi-radio",
							route: "#content/historical-cultural"
						},

						{
							caption: "Подієві",
							icon: "mdi-alarm",
							route: "#content/events-resourses"
						},

						{
							caption: "Соціально-економічні",
							icon: "mdi-human-male-female",
							route: "#content/social-economic"
						}
					]
				},
				{
					caption: "Методологічні аспекти",
					icon: "mdi-presentation",
					content: [
						{
							caption: "Історична довідка про ГІС",
							icon: "mdi-timer-sand",
							route: "#content/historical-background-ma"
						},

						{
							caption: "Визначення ГІС",
							icon: "mdi-comment-text",
							route: "#content/definition-ma"
						},

						{
							caption: "Програмне забезпечення ГІС",
							icon: "mdi-console",
							route: "#content/software-ma"
						},

						{
							caption: "Аналіз ГІС",
							icon: "mdi-loupe",
							route: "#content/analysis-ma"
						},

						{
							caption: "Область ГІС",
							icon: "mdi-bank",
							route: "#content/region-ma"
						},

						{
							caption: "Апаратні засоби ГІС",
							icon: "mdi-harddisk",
							route: "#content/hardware-ma"
						},

						{
							caption: "Функції ГІС",
							icon: "mdi-vector-combine",
							route: "#content/functions-ma"
						},

						{
							caption: "Принципи ГІС",
							icon: "mdi-book-open-variant",
							route: "#content/principles-ma"
						}
					]
				},
				{
					caption: "Методологічні дослідження",
					icon: "mdi-beaker",
					content: [
						{
							caption: "Дані в ГІС",
							icon: "mdi-database",
							route: "#content/data-in-gis"
						},

						{
							caption: "Подання інформації в ГІС",
							icon: "mdi-keyboard",
							route: "#content/introduction-gis"
						},

						{
							caption: "Аналітичні можливості ГІС",
							icon: "mdi-lightbulb",
							route: "#content/methodological-research"
						}
					]
				},
				{
					caption: "Довідка",
					icon: "mdi-help",
					content: [
						{
							caption: "Використані джерела",
							icon: "mdi-book-open-variant",
							route: "#content/literature",
							attr: "data-remodal-target='modal-literature'"
						},
						{
							caption: "Про програму",
							icon: "mdi-code-braces",
							route: "#content/help",
							attr: "data-remodal-target='modal-about'"
						},
						{
							caption: "Про автора",
							icon: "mdi-account-circle",
							route: "#content/about",
							attr: "data-remodal-target='modal-author'"
						}
					]
				}
			]
		}
	],
	/* END MAIN NAVIGATION-------
	---------------------------*/



	/* CONTENT NAVIGATION-------
	--------------------------*/	
	contentNavigation: [
		{
			"naturaly-climatic": [
				{
					caption: "Геологічні ресурси",
					route: "content/geological"
				},
				{
					caption: "Ландшафтні ресурси",
					route: "content/landscape"
				},
				{
					caption: "Флоро-фауністичні та лісові",
					route: "content/faunal"
				},
				{
					caption: "Кліматичні ресурси",
					route: "content/climatic"
				},
				{
					caption: "Погодні ресурси",
					route: "content/weather"
				},
				{
					caption: "Водні ресурси",
					route: "content/water"
				},
				{
					caption: "Бальнеологічні ресурси",
					route: "content/balneological"
				}
			],
			"naturaly-anthropogenic": [
				{
					caption: "Національні парки України",
					route: "",
					content:[
						{
							caption: "Карпатський національний природний парк",
							route: "content/carpathian-nnp"
						},
						{
							caption: "Національний природний парк «Шацький»",
							route: "content/shatsky-nnp"
						},
						{
							caption: "Національний природний парк «Нижньодністровський»",
							route: "content/lower-dniester-nnp"
						},
						{
							caption: "Національний природний парк «Святі гори»",
							route: "content/holy-mountain-nnp"
						},
						{
							caption: "Національний природний парк «Синевир»",
							route: "content/synevyr-nnp"
						},
						{
							caption: "Національний природний парк «Голосіївський»",
							route: "content/holosiivskyi-nnp"
						},
						{
							caption: "Національний природний парк «Гуцульщина»",
							route: "content/huzulschyna-nnp"
						},
						{
							caption: "Національний природний парк «Сколівські Бескиди»",
							route: "content/skole-beskids-nnp"
						},
						{
							caption: "Гетьманський національний природний парк",
							route: "content/hetman-nnp"
						},
						{
							caption: "Дермансько-острозький національний природний парк",
							route: "content/derman-ostrog-nnp"
						}
					]
				},
				{
					caption: "Природні та біосферні заповідники",
					route: "",
					content: [
						{
							caption: "Біосферний заповідник «Асканія-Нова»",
							route: "content/askania-nova-nbr"
						},
						{
							caption: "Асканійський цілинний заповідний степ",
							route: "content/askaniyske-nbr"
						},
						{
							caption: "Чорноморський біосферний заповідник",
							route: "content/black-sea-nbr"
						},
						{
							caption: "Карпатський біосферний заповідник",
							route: "content/carpathian-nbr"
						},
						{
							caption: "Дунайський біосферний заповідник",
							route: "content/danube-nbr"
						}
					]
				},
				{
					caption: "Заказники та пам'ятки природи",
					route: "",
					content: [
						{
							caption: "Загальна характеристика",
							route: "content/natural-reserves-monuments"
						},
						{
							caption: "Заказники загальнодержавного значення",
							route: "",
							content: [
								{
									caption: "Бакайський заказник",
									route: "content/bakayskyy-pni"
								},
								{
									caption: "Заказник «Березові гайки»",
									route: "content/birch-nuts-pni"
								},
								{
									caption: "Ягорлицький заказник",
									route: "content/yahorlyk-pni"
								},
								{
									caption: "Джарилгацький заказник",
									route: "content/dzharylgachsky-pni"
								},
								{
									caption: "Заказник «Саги»",
									route: "content/sahy-pni"
								}
							]
						},
						{
							caption: "Заказники місцевого значення",
							route: "",
							content: [
								{
									caption: "Заказник «Бакайський жолоб»",
									route: "content/bakayskyy-trough-lr"
								},
								{
									caption: "Заказник «Інгулецький лиман»",
									route: "content/inguletskiy-estuary-lr"
								},
								{
									caption: "Заказник «Хрестова сага»",
									route: "content/cross-saha-lr"
								},
								{
									caption: "Заказник «Шаби»",
									route: "content/shaby-lr"
								},
								{
									caption: "Заказник «Інгулець»",
									route: "content/inhulets-lr"
								},
								{
									caption: "Заказник «Корсунський»",
									route: "content/korsunskiy-lr"
								},
								{
									caption: "Заказник «Асканійський»",
									route: "content/askaniyske-lr"
								},
								{
									caption: "Заказник «Софіївський»",
									route: "content/sophia-lr"
								},
								{
									caption: "Заказник «Широка балка»",
									route: "content/wide-beam-lr"
								},
								{
									caption: "Заказник «Домузла»",
									route: "content/domuzla-lr"
								},
								{
									caption: "Заказник «Каїрська балка»",
									route: "content/cairo-beam-lr"
								}
							]
						}
					]
				},
				{
					caption: "Дендропарки, ботанічні сади, зоопарки",
					route: "",
					content:[
						{
							caption: "Дендрарій",
							route: "content/arboretum"
						},
						{
							caption: "Ботанічний сад",
							route: "content/botanical-garden"
						},
						{
							caption: "Зоопарки",
							route: "",
							content: [
								{
									caption: "Ялтинський зоопарк «Казка»",
									route: "content/yalta-zoos"
								},
								{
									caption: "Миколаївський зоопарк",
									route: "content/mykolayiv-zoos"
								},
								{
									caption: "Одеський зоопарк",
									route: "content/odessa-zoos"
								},
								{
									caption: "Харківський зоопарк",
									route: "content/kharkov-zoos"
								},
								{
									caption: "Луцький зоопарк",
									route: "content/lutsk-zoos"
								}
							]
						},
						{
							caption: "11 найкрасивіших дендропарків України",
							route: "",
							content: [
								{
									caption: "Дендропарк «Софіївка»",
									route: "content/sophia-bpu"
								},
								{
									caption: "Дендропарк «Олександрія»",
									route: "content/alexandria-bpu"
								},
								{
									caption: "Дендропарк «Тростянець»",
									route: "content/trostianets-bpu"
								},
								{
									caption: "Кіровоградський дендропарк",
									route: "content/kirovograd-bpu"
								},
								{
									caption: "Чернівецький дендропарк",
									route: "content/chernivtsi-bpu"
								},
								{
									caption: "Сторожинецький дендропарк",
									route: "content/storozhynets-bpu"
								},
								{
									caption: "Березнівський дендропарк",
									route: "content/bereznovsky-bpu"
								},
								{
									caption: "Дендропарк «Веселі боковеньки»",
									route: "content/funny-bokovenki-bpu"
								},
								{
									caption: "Полтавський дендропарк",
									route: "content/poltava-bpu"
								},
								{
									caption: "Клесівський дендропарк",
									route: "content/klesivsky-bpu"
								},
								{
									caption: "Краснокутський дендропарк",
									route: "content/krasnokutskiy-bpu"
								}
							]
						}
					]
				}
			],
			"historical-cultural": [
				{
					caption: "Пам'ятки археології",
					route: "content/archaeology-hc"
				},
				{
					caption: "Пам'ятки історії",
					route: "",
					content: [
						{
							caption: "Києво-Печерська лавра",
							route: "content/kyivo-pecherska-lavra-mh"
						},
						{
							caption: "Державний історико-архітектурний заповідник «Хотинська фортеця»",
							route: "content/khotyn-fortress-mh"
						},
						{
							caption: "Софія Київська",
							route: "content/sophia-kyiv-mh"
						},
						{
							caption: "Херсонес Таврійський",
							route: "content/chersonesos-mh"
						},
						{
							caption: "Національний історико-архітектурний заповідник «Кам’янець»",
							route: "content/reserve-kamenetz-mh"
						}
					]
				},
				{
					caption: "Архітектурні пам'ятки та пам'ятки містобудування",
					route: "content/architectural-hc"
				},
 				{
					caption: "Пам'ятки мистецтва",
					route: "content/arts-hc"
				},
				{
					caption: "Етнографічні пам'ятки",
					route: "content/ethnographic-hc"
				},
				{
					caption: "Топ 9 об'єктів Волині",
					route: "",
					content:[
						{
							caption: "Замок Любарта",
							route: "content/castle-westminster-vhc"
						},
						{
							caption: "Музей бурштину",
							route: "content/amber-museum-vhc"
						},
						{
							caption: "Кременець, Почаїв",
							route: "content/kremenets-pochaev-vhc"
						},
						{
							caption: "Берестечко",
							route: "content/berestechko-vhc"
						},
						{
							caption: "Музей Лесі Українки",
							route: "content/museum-of-ukrainian-lesya-vhc"
						},
						{
							caption: "Зимненський Свято-Успенський Святогірський жіночий монастир",
							route: "content/zymne-monastery-vhc"
						},
						{
							caption: "Oстрозький замок",
							route: "content/ostroh-castle-vhc"
						},
						{
							caption: "Дубенський замок",
							route: "content/dubno-castle-vhc"
						},
						{
							caption: "Пересопниця",
							route: "content/peresopnytsya-vhc"
						}
					]
				}
			],
			"events-resourses":[
			],
			"social-economic": [
				{
					caption: "Транспортна мережа",
					route: "content/transport-network-sc"
				},
				{
					caption: "Населення",
					route: "content/people-sc"
				}
			],
			"methodological-aspects": [
				{
					caption: "Історична довідка про ГІС",
					route: "content/historical-background-ma"
				},
				{
					caption: "Визначення ГІС",
					route: "content/definition-ma"
				},
				{
					caption: "Програмне забезпечення ГІС",
					route: "content/software-ma"
				},
				{
					caption: "Аналіз ГІС",
					route: "content/analysis-ma"
				},
				{
					caption: "Область ГІС",
					route: "content/region-ma"
				},
				{
					caption: "Апаратні засоби ГІС",
					route: "content/hardware-ma"
				},
				{
					caption: "Функції ГІС",
					route: "content/functions-ma"
				},
				{
					caption: "Принципи ГІС",
					route: "content/principles-ma"
				}
			],
			"data-in-gis": [
				{
					caption: "Просторова інформація в ГІС",
					route: "content/data-in-gis"
				},
				{
					caption: "Географічні дані в ГІС",
					route: "content/geography-data-dig"
				},
				{
					caption: "Атрибутні дані в ГІС",
					route: "content/attributive-data-dig"
				},
				{
					caption: "Моделі і бази даних в ГІС",
					route: "content/models-dig"
				}
			],
			"introduction-gis": [
				{
					caption: "Автоматизоване введення даних",
					route: "content/introduction-gis"
				},
				{
					caption: "Векторизування",
					route: "content/vectorization-ig"
				},
				{
					caption: "Геокодування",
					route: "content/geocoding-ig"
				},
				{
					caption: "Ручне введення даних",
					route: "content/manual-data-entry-ig"
				},
				{
					caption: "Контроль якості створення цифрових карт",
					route: "content/quality-control-ig"
				},
				{
					caption: "Подання інформації в ГІС",
					route: "content/presentation-ig"
				}
			],
			"methodological-research": []
		}
	],
	/* END CONTENT NAVIGATION------
	-----------------------------*/



	/* CONTENT PARTS-----
	-------------------*/
	"naturaly-climatic": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-climatic"
			]
		}
	],
	"naturaly-anthropogenic": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"historical-cultural": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"events-resourses": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"events-resourses"
			]
		}
	],
	"social-economic": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"social-economic"
			]
		}
	],
	"historical-background-ma": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"methodological-aspects"
			]
		}
	],
	"definition-ma": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"methodological-aspects"
			]
		}
	],
	"software-ma": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"methodological-aspects"
			]
		}
	],
	"analysis-ma": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"methodological-aspects"
			]
		}
	],
	"region-ma": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"methodological-aspects"
			]
		}
	],
	"hardware-ma": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"methodological-aspects"
			]
		}
	],
	"functions-ma": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"methodological-aspects"
			]
		}
	],
	"principles-ma": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"methodological-aspects"
			]
		}
	],
	"data-in-gis": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"data-in-gis"
			]
		}
	],
	"introduction-gis": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"introduction-gis"
			]
		}
	],
	"methodological-research": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"methodological-research"
			]
		}
	],
	"geological": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-climatic"
			]
		}
	],
	"landscape": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-climatic"
			]
		}
	],
	"faunal": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-climatic"
			]
		}
	],
	"climatic": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-climatic"
			]
		}
	],
	"weather": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-climatic"
			]
		}
	],
	"water": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-climatic"
			]
		}
	],
	"balneological": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-climatic"
			]
		}
	],
	"carpathian-nnp": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"shatsky-nnp": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"lower-dniester-nnp": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"holy-mountain-nnp": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"synevyr-nnp": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"holosiivskyi-nnp": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"huzulschyna-nnp": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"skole-beskids-nnp": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"hetman-nnp": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"derman-ostrog-nnp": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"askania-nova-nbr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"askaniyske-nbr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"black-sea-nbr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"carpathian-nbr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"danube-nbr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"natural-reserves-monuments": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"bakayskyy-pni": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"birch-nuts-pni": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"yahorlyk-pni": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"dzharylgachsky-pni": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"sahy-pni": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"bakayskyy-trough-lr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"inguletskiy-estuary-lr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"cross-saha-lr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"shaby-lr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"inhulets-lr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"korsunskiy-lr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"askaniyske-lr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"sophia-lr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"wide-beam-lr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"domuzla-lr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"cairo-beam-lr": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"arboretum": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"botanical-garden": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"yalta-zoos": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"mykolayiv-zoos": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"odessa-zoos": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"kharkov-zoos": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"lutsk-zoos": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"sophia-bpu": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"alexandria-bpu": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"trostianets-bpu": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"kirovograd-bpu": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"chernivtsi-bpu": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"storozhynets-bpu": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"bereznovsky-bpu": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"funny-bokovenki-bpu": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"poltava-bpu": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"klesivsky-bpu": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"krasnokutskiy-bpu": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"naturaly-anthropogenic"
			]
		}
	],
	"archaeology-hc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"kyivo-pecherska-lavra-mh": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"khotyn-fortress-mh": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"sophia-kyiv-mh": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"chersonesos-mh": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"reserve-kamenetz-mh": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"architectural-hc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"arts-hc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"ethnographic-hc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"castle-westminster-vhc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"amber-museum-vhc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"kremenets-pochaev-vhc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"berestechko-vhc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"museum-of-ukrainian-lesya-vhc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"zymne-monastery-vhc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"ostroh-castle-vhc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"dubno-castle-vhc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"peresopnytsya-vhc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"historical-cultural"
			]
		}
	],
	"transport-network-sc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"social-economic"
			]
		}
	],
	"people-sc": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"social-economic"
			]
		}
	],
	"spatial-information-dig": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"data-in-gis"
			]
		}
	],
	"geography-data-dig": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"data-in-gis"
			]
		}
	],
	"attributive-data-dig": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"data-in-gis"
			]
		}
	],
	"models-dig": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"data-in-gis"
			]
		}
	],
	"automated-data-entry-ig": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"introduction-gis"
			]
		}
	],
	"vectorization-ig": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"introduction-gis"
			]
		}
	],
	"geocoding-ig": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"introduction-gis"
			]
		}
	],
	"manual-data-entry-ig": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"introduction-gis"
			]
		}
	],
	"quality-control-ig": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"introduction-gis"
			]
		}
	],
	"presentation-ig": [
		{
			"textContent": []
		},
		{
			"mediaContent": []
		},
		{
			"gmaps": []
		},
		{
			"fullPage": []
		},
		{
			"contentNavigation": [
				"introduction-gis"
			]
		}
	]

};

module.exports = data;
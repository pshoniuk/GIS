var $ = require("jquery"),
	_ = require("underscore"),
	GMaps = require("gmaps"),
	nodePath = require("path"),
	Backbone = require("backbone"),
	templates = require("../source/templates"),
	remodal = require("../remodal.min"),
	mCustomScrollbar = require("malihu-custom-scrollbar-plugin")($),
	readDir = require("../source/readDir"),
	fullpage = require("fullpage.js"),
	traverse = require("traverse"),
	data = require("./data.js");



function getPath(){
	return nodePath.resolve(nodePath.join.apply(nodePath, arguments));
}

function getFirstKey(obj) {
	var key;
	for(key in obj){
		if(obj.hasOwnProperty(key)){
			return key;
		}
	}
	return null;
}

function getTitle(pointName) {
	var sectionTitle = "";

	if(getTitle.cache[pointName] == null) {
		traverse(data.contentNavigation).forEach(function (item) {
			if(item && item.route === "content/" + pointName) {
				sectionTitle = item.caption || "";
				this.stop();
			}
		});
		getTitle.cache[pointName] = sectionTitle;
	}
	return getTitle.cache[pointName];
}

getTitle.cache = Object.create(null);



module.exports = (function(){
	var app = {};
	app._public = {};
	app.models = {};
	app.views = {};
	Backbone.$ = $;

	_const = {
		"resPath"	 : "/resourses/",
		"bgExt"		 : "jpg" 
	};


	app.$ = {
		"body": $(document.body)
	};


	var createSection = function(content){
		var rootTpl = templates.get("fullpage.section.tpl"),
			introTpl = templates.get("fullpage.intro.tpl"),
			$root = $(rootTpl()).
				append(introTpl({content: content}));
		app.$.body.append($root);
		return $root;
	};


	//MAIN MENU
	app.models.MainMenu = Backbone.Model.extend({});


	app.views.MainMenu = Backbone.View.extend({
		className: "main-menu",
		tagName: "ul",
		events: {
			"click .item": "selectItem"
		},


		initialize: function(){
			this.listenTo(this.model, {
				"change:data": this.render,
				"destroy": this.destroy
			});
			this.render();
		},


		render: function(){
			var i, j, item, subItem, $item, l, l2,
				data = this.model.attributes.data,
				itemTpl = templates.get("mainMenu.item.tpl"),
				subItemTpl = templates.get("mainMenu.subItem.tpl");

			if(!data || typeof data !== "object"){
				return;
			}
			l = data.length;
			for(i = 0; i < l; i++){
				item = data[i];
			 	try{
			 		$item = $(itemTpl(item));
			 		this.$el.append($item);
			 	}catch(error){
			 		item = null;
			 	}
			 	if(item && item.content){
			 		$item = $item.find(".sub-items");
			 		l2 = item.content.length;
			 		for(j = 0; j < l2; j++){
			 			subItem = item.content[j];
					 	try{
							subItem.attr || (subItem.attr = "");
					 		$item.append(subItemTpl(subItem));
					 	}catch(error){}
			 		}
			 	}
			}
			return this.$el;
		},


		destroy: function(){
			this.stopListening();
			this.remove();
		},


		selectItem: function(e){
			var subItems, hash, $item,
				target = $(e.target);

			if(target.closest(".sub-items").length){
				$item = target.closest(".sub-item");
				hash = $item.find("a").attr("href");
				if(hash){
					window.location.hash = hash.substr(1);
				}
				return;
			}

			e.preventDefault();
			$item = target.closest(".item")
			$subItems = $item.find(".sub-items");
			this.$el.find(".item").not($item).removeClass("selected");
			if ($subItems.hasClass("hide")){
				$item.addClass("selected");
				this.$el[$subItems.find("a").length ? "addClass" : "removeClass"]("move");
			}else{
				$item.removeClass("selected");
				this.$el.removeClass("move");
			}
			this.$el.find(".sub-items").not($subItems).addClass("hide");
			$subItems.toggleClass("hide");
			return false;
		}
	});


	app._public.mainMenu = function(pointName){
		var args;
		args = Array.prototype.slice.call(arguments, 1);
		this.__proto__ = app._public.mainMenu.prototype;
		this.model = new app.models.MainMenu({data: args});
		view = new app.views.MainMenu({model: this.model});
		app.$.body.append(view.el);
		return this;
	};

	app._public.mainMenu.prototype.destroy = function(pointName){
		this.model.destroy();
	};



	//background
	app._public.bg = function(pointName, bgPath){
		bgPath = bgPath || window.gisRootPath + _const.resPath + pointName + "/bg." + _const.bgExt;
		app.$.body.css("background-image", "url(" + bgPath + ")");
	};


	app.regExp = {
		isVideo: /(\.mp4)$/i,
		isImage: /(\.jpg)|(\.png)|(\.gif)|(\.jpeg)|(\.bmp)$/i
	};

	//media content
	app._public.mediaContent = function(pointName){
		this.__proto__ = app._public.mediaContent.prototype;

		var rootTpl = templates.get("fullpage.section.tpl"),
			slideTpl = templates.get("fullpage.slide.tpl"),
			mediaImgTpl = templates.get("fullpage.media.img.tpl"),
			mediaVideoTpl = templates.get("fullpage.media.video.tpl"),
			$root = this.$root = $(rootTpl()),
			path = getPath(window.gisRootPath, _const.resPath, pointName, "/media/"),
			filesList = readDir.getListFiles(path),
			descFile = readDir.getFile(nodePath.join(path, "/description.dat")),
			sectionTitle = getTitle(pointName),
			media, i, e, l, videoContent, description;

		try{
			description = JSON.parse(descFile);
		}catch (e){
			description = {};
		}

		description || (description = {});
		for(i = 0, l = filesList.length; i < l; i++){
			videoContent = false;
			media = null;

			if(app.regExp.isVideo.test(filesList[i])){
				media = mediaVideoTpl({
					src: nodePath.join(path, filesList[i])
				});
				videoContent = true;
			}else if(app.regExp.isImage.test(filesList[i])){
				media = mediaImgTpl({
					src: nodePath.join(path, filesList[i])
				});
			}

			if(media) {
				$root.append(slideTpl({
					media: media,
					description: description[filesList[i].replace(/[.][^.]+$/, "")] || description.__general ||  sectionTitle
				}));
			}
		}
		if(filesList.length){
			app.$.body.append($root);
		}
		return this;
	};

	app._public.mediaContent._cache = Object.create(null);
	app._public.mediaContent.prototype.destroy = function(){
		this.$root.remove();
	};


	//text content
	app._public.textContent = function(pointName, text){
		var header, title;

		this.__proto__ = app._public.textContent.prototype;
		text || (text = readDir.getFile(getPath(window.gisRootPath, _const.resPath, pointName, "./textContent.tpl")));
		this.$root = createSection(text);
		header = this.$root.find(".body").prev("h1");

		if(header.length && !header.hasClass("important") && (title = getTitle(pointName))) {
			header.remove();
			this.$root.find(".body").before("<h1>" + title + "</h1>");
		}

		this.$root.addClass("gis-text-content").find(".intro").css("height", app.$.body.height() - 100 + "px");
		// $(document.body).append("<h1 class='debug-message'>" + nodePath.join(pointName, "./textContent.tpl") + "</h1>");
		return this;
	};

	app._public.textContent.prototype.destroy = function(){
		this.$root.remove();
	};


	var imc = 0;
	//Google Maps
	app._public.gmaps = function(pointName, data){
		this.__proto__ = app._public.gmaps.prototype;
		var gMapData = readDir.getFile(getPath(window.gisRootPath , _const.resPath , pointName , "./gmaps.dat"));
		if(gMapData){
			gMapData = JSON.parse(gMapData);
			this.$root = createSection("");
			var $gmapEl = this.$root.children().eq(0);
			var gmapEl = $gmapEl[0];
			var center = (gMapData.paths && gMapData.marker ? gMapData.mapGeneral : gMapData.marker);
			center = $.extend(true, {}, center);

			this.gmap = gmap = new GMaps({
				el: gmapEl,
				lat: center.lat,
				lng: center.lng,
				scrollwheel: false,
				mapType: "hybrid",
				zoom: gMapData.mapGeneral.zoom,
				zoomControl : true,
				zoomControlOpt: {
					style : "SMALL",
					position: "RIGHT_BOTTOM"
				},
				mapTypeControlOptions: {
					position: google.maps.ControlPosition.TOP_CENTER
				},
				panControl : false,
				streetViewControl : true,
				mapTypeControl: true,
				overviewMapControl: true
			});

			if(gMapData.paths){
				for(var i = 0, l = gMapData.paths.length; i < l; i++){
					var path = gMapData.paths[i];
					gmap.drawPolygon({
						paths: path,
						strokeColor: '#BBD8E9',
						strokeOpacity: 1,
						strokeWeight: 3,
						fillColor: '#BBD8E9',
						fillOpacity: 0.6
					});
				}
			}else if(gMapData.marker){
				gmap.addMarker({
					lat: gMapData.marker.lat,
					lng: gMapData.marker.lng,
					title: gMapData.caption
				});
			}

			$gmapEl.css({
				"display": "block",
				"overflow"	: "hidden",
				"height"	: "100%",
				"width"		: "100%",
				"left"		: 0,
				"top"		: 0
			});

			setTimeout(function(){
				gmap.refresh();
				gmap.setCenter(gMapData.marker);
			}, false);

			imc++;
		}
		return this;
	};

	app._public.gmaps.prototype.destroy = function(){
		if(this.gmap){
			this.gmap = null;
			this.$root.remove();
		}
	};



	//Full Page
	app._public.fullPage = function(pointName){
		this.__proto__ = app._public.fullPage.prototype;
		var wrap = this.wrap = $("<div></div>", {
			"class": "wrap-content"
		}).append(app.$.body.find(">.section"));
		app.$.body.append(wrap);
		//create fullpage
		wrap.fullpage({
			"navigationPosition"	: "right",
			"slidesNavPosition"		: "bottom",
			"verticalCentered"		: true,
			"slidesNavigation"		: true,
			"scrollOverflow"		: false,
			"loopHorizontal"		: false,
			"scrollingSpeed"		: 1000,
			"sectionsColor"			: ["rgba(0,0,0,.7)", "rgba(0,0,0,.7)", "rgba(0,0,0,.7)"],
			"navigation"			: true,
			"resize"				: true
		});

		wrap.find(".gis-text-content .intro").mCustomScrollbar({
			theme: "dark-3",
			scrollInertia: 200
		});

		wrap.find(".ignore-mouse-wheel").on("mousewheel", function(){
			return false;
		});

		return this;
	};

	app._public.fullPage.prototype.destroy = function(){
		$.fn.fullpage.destroy("all");
		this.wrap.remove();
	};



	//Content Navigation
	var ContentNavigation = Backbone.View.extend({
		"className"	: "menu",
		"tagName"	: "div",
		"events"	: {
			"click .content-link"	: "showContent",
			"click .menu-button"	: "closeMenu",
			"click .item:not('.disabled')"			: "selectItem",
			"click .circle"			: "mainRedirect"
		},

		initialize: function(menuData){
			var self = this;
			this.menuData = menuData || [];
			this.tplRoot = templates.get("contentNavigation.root.tpl");
			this.tplLeftPreview = templates.get("contentNavigation.leftPreview.tpl");
			this.tplRightPreview = templates.get("contentNavigation.rightPreview.tpl");
			this.render(menuData);
			setTimeout(function(){
				var wrapper = self.$el.find(".sub-items-wrapper");
				wrapper.mCustomScrollbar({
					theme: "dark-3",
					scrollInertia: 200
				});
				self.$el.on("mousewheel", function(){ return false; });

				var activeEl = self.$("[href='" + window.location.hash + "']"),
					activeSection = activeEl.parents(".sub-items-wrapper"),
					contentLink;

				if(activeSection.length) {
					contentLink =  activeEl.closest(".content").prev(".content-link");
					self.selectItem(null, activeSection[0].parentItem);
					self.showContent(null, contentLink);
					wrapper.mCustomScrollbar("scrollTo", activeEl);
				} else {
					self.selectItem(null, activeEl);
				}
			}, false);
		},

		render: function(){
			var i, j, k, l, l2, l3, item, subItem, ssItem, leftPreviewWrap,
				rightPreviewWrap, subItems, $subItems, listItem, hash, menuData, parentItem;

			menuData = this.menuData;
			hash = (window.location.hash || "").substr(1);

			this.$el.html(this.tplRoot);
			this.$(".disabled").removeClass("disabled");
			leftPreviewWrap = this.$el.find(".left-preview");
			rightPreviewWrap = this.$el.find(".right-preview");

			for(i = 0, l = menuData.length; i < l; i++){
				item = menuData[i];
				leftPreviewWrap.append(this.tplLeftPreview({
					"caption"	: item.caption,
					"route"		: item.route,
					"disabled": hash === item.route
				}));

				if(item.content){
					subItems = "";
					for(j = 0, l2 = item.content.length; j < l2; j++){
						subItem = item.content[j];
						subItems += this.tplRightPreview({
							"caption": subItem.caption,
							"content": (subItem.content ? "content-link" : ""),
							"route": subItem.route,
							"disabled": hash === subItem.route
						});

						if(subItem.content){
							listItem = "";
							subItems += "<div class='content'>";
							for(k = 0, l3 = subItem.content.length; k < l3; k++){
								ssItem = subItem.content[k];
								subItems += this.tplRightPreview({
									"caption": ssItem.caption,
									"content": "",
									"route": ssItem.route,
									"disabled": hash === ssItem.route
								});
							}
							subItems += "</div>";
						}
					}

					$subItems = $("<div class='sub-items-wrapper'>" + subItems + "</div>");
					rightPreviewWrap.append($subItems);
					parentItem = leftPreviewWrap.children().last()[0];
					parentItem.items = $subItems;

					$subItems.each((index, item) => {
						item.parentItem = parentItem;
					});
				}
			}
			return this.$el;
		},

		closeMenu: function(e){
			var menu;
			if(!$(e.target).closest("a").hasClass("menu-button")){
				return;
			}
			e.preventDefault();
			menu = this.$el;
			setTimeout(function (){
				menu.css("z-index", "-1");
			}, 400);
			menu.removeClass("in");
		},

		mainRedirect: function(e){
			e.preventDefault();
			window.location.hash = "main";
			return false;
		},


		selectItem: function(e, target){
			var parentLi, link, $subItems, hWrapper, hSubItems, $items;
			parentLi = $(target || e.target).closest("li.item");
			link = parentLi.find(">a");
			this.$(".item.disabled").removeClass("disabled");

			if($.trim(link.attr("href")) === "#"){
				e && e.preventDefault();
				$subItems = parentLi[0].items;
				parentLi.addClass("disabled");
				this.$(".sub-items-wrapper").removeClass("active");
				$subItems.addClass("active");
				$items = $subItems.find(".sub-item");
				hWrapper = $subItems.height();
				hSubItems = $items.eq(0).height()*$items.length;
				if(hWrapper > hSubItems){
					$subItems.css({
						"padding-top": (hWrapper - hSubItems)/2 + "px"
					});
				}else{
					$subItems.css({
						"padding-top": ""
					});
				}
				return false;
			} else {
				parentLi.addClass("disabled");
				if(!target) window.location.hash = link.attr("href").substr(1);
			}
		},

		showContent: function (e, link) {
			e && e.preventDefault();
			link || (link = $(e.target).closest(".content-link"));
			link.next("div.content").toggleClass("active");
		}
	});

	
	var MenuButton = Backbone.View.extend({
		"className"	: "menu-button",
		"tagName"	: "a",
		"events"	: {
			"click i" : "openMenu"
		},

		initialize: function(menu){
			this.menu = menu;
			this.render();
		},

		render: function(){
			this.$el.html("<i class='mdi mdi-menu'></i>");
			this.$el.attr("href", "#");
		},

		openMenu: function(e){
			var menu;
			if(!$(e.target).closest("a").hasClass("menu-button")){
				return;
			}
			e.preventDefault();
			menu = this.menu;
			menu.css("z-index", "999999");
			menu.addClass("in");
		}
	});


	app._public.contentNavigation = function(pointName, key){
		var menuData = data.contentNavigation[0][key];
		this.__proto__ = app._public.contentNavigation.prototype;
		this.cnView = new ContentNavigation(menuData);
		this.mbView = new MenuButton(this.cnView.$el);
		app.$.body.append(this.cnView.$el);
		app.$.body.append(this.mbView.$el);
		return this;
	};

	app._public.contentNavigation.prototype.destroy = function(){
		this.cnView.remove();
		this.mbView.remove();
	};



	return app._public;
})();
<h1>
Погодні ресурси
</h1>
<div class='body'>
<p>	В Україні чітко виражена зміна сезонів протягом року. Взимку над Україною розвивається циклонічна діяльність, повітряні маси часто змінюються, 
тому погода не стійка. В середньому за зиму буває до десяти відлиг. Зима триває від 120-130 днів на північному сході і до 75-85 днів на південному заході. 
Весняний період закінчується переходом середньодобової температури повітря через 15°С. Літо на більшій частині території тепле, а на півдні України жарке. У 
степовій зоні він триває 120-130 днів. Влітку випадає близько 40% річної суми опадів. Закінчується літній період після переходу температури повітря через 15°С в 
бік зниження, що відбувається в першій декаді вересня. З настанням осені зростає циклонічна діяльність, відбувається зниження температури повітря, збільшується 
кількість днів з опадами і туманами. У вересні і жовтні можливі заморозки. Проте буває короткочасне повернення тепла з сонячною лагідною погодою.
<p>	</div>

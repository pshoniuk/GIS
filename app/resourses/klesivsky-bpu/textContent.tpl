<h1>
Клесівський
</h1>
<div class='body'>
<p>	СМТ Клесів Рівенської області. Територія – 3 га.
<p> 
Дендропарк молодий (1997рік заснування), невеликий, але надзвичайно миловидний. Найперше відомий своїми «зеленими 
скульптурами». Їхньої краси неможливо передати через фото, їх треба побачити. Найрізноманітніші несподівані фігури створені 
лише за допомогою ножиць та уяви автора.
<p> 
Серед найскладніших скульптур — Свічка Діви Марії, Олімпійська чаша, Кубок Богдана Хмельницького. А біля входу відвідувачів 
зустрічає величезний герб України з кулястих туй.
<p> 
Каштанова алея, арки з рослин, рукотворні озера, мости та острівки єднання, кохання і дружби.
<p> 
Творцем Клесівського дендропарку є лише одна людина — геніальний дизайнер-самоучка Олексій Ворон.
<p>	</div>

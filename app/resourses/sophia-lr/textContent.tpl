<h1>
Софіївський
</h1>
<div class='body'>
<p>	Ботанічний заказник площею 194га. Розташований на південно- західній околиці с. Софіївки Білозерського району та прилеглій акваторії 
Дніпровського лиману. Охороняються комплекс цілинного степу на лесових зсувах та водні фітоценози верхньої частини Дніпровського лиману. В степовій балці та на 
надлиманних горбах збереглася типова степова рослинність типчаково-ковилових степів з багатим степовим різнотрав'ям з участю рідкісних рослин — ковили української 
та волосистої, тюльпана бузького та Шренка, сальвінії плаваючої, рідкісних тварин — п'явки медичної, полоза жовточеревого, поліксени. Охороняються також рідкісні 
рослинні формації, що включені до Зеленої книги України — формація латаття білого, глечиків жовтих, сальвінії плаваючої, ковили волосистої. Заказник виконує 
важливу роль як частина екологічного коридору.
<p>	</div>

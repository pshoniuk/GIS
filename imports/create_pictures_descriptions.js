const fs = require("fs"),
	path = require("path"),
	nodeWatch = require("node-watch"),
	resPath = path.resolve("../app/resourses/");


function log(message, color) {
	console.log((color || "") + message + "\x1b[0m");
}

function errorLog(message) {
	log(message, "\x1b[31m");
}

function successLog(message) {
	log(message, "\x1b[32m");
}


function updateDescriptions() {
	fs.readdir(resPath, (err, list) => {
		if(err) return errorLog(err);

		for(let mediaDir of list) {
			mediaDir = path.join(resPath, mediaDir);
			const mediaPath = path.join(mediaDir, "media"),
				descPath = path.join(mediaPath, "description.dat");

			if(fs.statSync(mediaDir).isDirectory() && fs.existsSync(mediaPath)) {
				const mediaFiles = Object.create(null);
				let prevData = Object.create(null);

				if(fs.existsSync(descPath)){
					let e;
					try {
						prevData = JSON.parse(fs.readFileSync(descPath));
					} catch (e) {
						errorLog(e);
					}
				}

				fs.readdir(mediaPath, (err, list)=>{
					if(err) return errorLog(err);

					for(let mediaFile of list) {
						mediaFile = path.join(mediaPath, mediaFile);
						const fileName = path.basename(mediaFile).replace(/[.][^.]+$/, "");
						if(fs.statSync(mediaFile).isFile()) {
							mediaFiles[fileName] = prevData[fileName] || "";
						}
					}

					delete mediaFiles.description;
					mediaFiles.__general = prevData.__general;
					if(JSON.stringify(prevData) !== JSON.stringify(mediaFiles)) {
						fs.writeFile(descPath, JSON.stringify(mediaFiles, null, 4), (err) => {
							if(err) return errorLog(err);
							successLog(`Write description.data in ${ mediaPath }`);
						});
					}
				});
			}
		}
	});
}


updateDescriptions();
if(process.argv.indexOf("--watch") !== -1) nodeWatch(resPath, updateDescriptions);

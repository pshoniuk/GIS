﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GIS
{
    public partial class Form27 : Form
    {
        public Form27()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form28 askania = new Form28();
            askania.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form29 aska = new Form29();
            aska.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form30 blacksea = new Form30();
            blacksea.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form31 karpat = new Form31();
            karpat.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form32 dunay = new Form32();
            dunay.ShowDialog();
        }

       
    }
}

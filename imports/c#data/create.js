var fs = require("fs");
var readDir = {
	getListFiles: function(path){
		var list = [], e;
		if(!path) return list;
		try{
			return fs.readdirSync(path);
		}catch(e){
			return list;
		}
	},

	getFile: function(fileName){
		var file = "", e;
		if(!fileName) return ;
		try{
			file = fs.readFileSync(fileName, "utf8");
		}catch(e){}
		return file;
	},

	getFiles: function(path){
		var list = [], files = [], i, fileName, e;
		if(!path) return list;
		list = this.getListFiles(path);
		if(!list){
			return null;
		}
		for(i = 0; i < list.length; i++){
			fileName = list[i];
			try{
				files.push(fs.readFileSync(path + fileName, "utf8"));
			}catch(e){}
		}
		return files;
	}
};



var files = readDir.getFiles(process.cwd() + "\\");
var regExp = /(resource\.Text(\s*)=(\s*)"([\s\S]*?)";)[\s\S]*?(resource\.res_head(\s*)=(\s*)"([\s\S]*?)";)[\s\S]*?(resource\.res_top(\s*)=(\s*)"([\s\S]*?)";)[\s\S]*?(resource\.res_bot(\s*)=(\s*)"([\s\S]*?)";)/gmi;
var replaceChar = /["+]|(\\")/gm;
var replaceParagraph = /\\n\\n/gm;
var writeFile = fs.openSync(process.cwd() + "\\data.html", "w");
var blockCounter = 0;


var toUpperCase = function(str){
	str.replace(/[a-zа-я]/, function(c){
		return c.toUpperCase();
	});
	return str;
};


var json = require("../../app/js/source/data.js");
var lastCaption = ""
var aliases = {};


var recursive = function(obj){
	var key, val;
	if(obj && typeof obj === "object"){
		for(key in obj){
			val = obj[key];
			recursive(val);
			if(key === "caption"){
				lastCaption = val.trim().toLowerCase().replace(replaceChar, "");
			}else if(key === "route" && lastCaption){
				aliases[lastCaption] = val; 
			}
		}
	}
};

recursive(json);
console.log("ALL ALIASES: ", JSON.stringify(aliases, null, 4));
var counterDefinedTpl = 0;

files.forEach(function(page){
	var block, head, top, bottom, tpl, alias;
	while(block = regExp.exec(page)){
		head = toUpperCase(block[4]).replace(replaceChar, "");
		top = toUpperCase(block[12]).replace(replaceChar, "").replace(replaceParagraph, "\n\t<p> ");
		bottom = toUpperCase(block[16]).replace(replaceChar, "").replace(replaceParagraph, "\n\t<p> ");
		tpl = "<h1>\n\t" + head + "\n</h1>\n<div class='body'>\n\t<p>\t" + top + "\n\t<p>\t" + bottom + "</div>\n\n\n\n";
		fs.writeSync(writeFile, tpl);
		alias = aliases[head.trim().toLowerCase()];
		if(alias){
			+function(alias, head, tpl){
				fs.open("../../app/resourses/" + alias.replace(/#?content\//i, "") + "\\textContent.tpl", "w", function(err, file){
					if(!err){
						fs.write(file, tpl, function(){});
						console.log(++counterDefinedTpl, head, alias);
					}
				});
			}(alias, head, tpl);
			
		}
		blockCounter++;
	}
});

console.log("\n\nTemplates" + blockCounter);
fs.closeSync(writeFile);
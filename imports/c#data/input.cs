﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GIS
{
    public partial class input : Form
    {
        public input()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Векторизування";
            string path = Application.StartupPath + @"\src\html\b2102.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\doslid.jpg");
            browse.ShowDialog();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Геокодування";
            string path = Application.StartupPath + @"\src\html\b2103.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\doslid.jpg");
            browse.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Ручне введення даних. Апаратне та екранне дигітизування";
            string path = Application.StartupPath + @"\src\html\b2104.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\doslid.jpg");
            browse.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Контроль якості створення цифрових карт";
            string path = Application.StartupPath + @"\src\html\b2105.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\doslid.jpg");
            browse.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Автоматизоване введення даних";
            string path = Application.StartupPath + @"\src\html\b2101.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\doslid.jpg");
            browse.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Подання інформації в ГІС";
            string path = Application.StartupPath + @"\src\html\b2106.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\doslid.jpg");
            browse.ShowDialog();
        }
    }
}

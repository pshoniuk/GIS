﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;

namespace GIS
{
    public partial class Form26 : Form
    {
        public Form26()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            slideshow picture = new slideshow();
            picture.Text = "Дермансько-Острозький національний природний парк";
            picture.img = pictureBox1.Image;
            picture.ShowDialog();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            slideshow picture = new slideshow();
            picture.Text = "Дермансько-Острозький національний природний парк";
            picture.img = pictureBox2.Image;
            picture.ShowDialog();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            maps maps = new maps();
            maps.Text = "Дермансько-Острозький національний природний парк";
            maps.center = new GMap.NET.PointLatLng(50.291253, 26.242454);
            maps.zoom = 12; maps.pos = 12;

            GMapOverlay markersOverlay = new GMapOverlay("markers");
            GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(50.291253, 26.242454),
              GMarkerGoogleType.green);
            markersOverlay.Markers.Add(marker);
            maps.map.Overlays.Add(markersOverlay);

            maps.ShowDialog();
        }

    }
}

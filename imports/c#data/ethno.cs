﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GIS
{
    public partial class ethno : Form
    {
        public ethno()
        {
            InitializeComponent();
        }

        public string title_txt
        {
            get { return title.Text; }
            set { title.Text = value; }
        }

        public string main_txt
        {
            get { return text.Text; }
            set { text.Text = value; }
        }

        public bool up_en
        {
            get { return up_but.Visible; }
            set { up_but.Visible = value; }
        }

        public bool down_en
        {
            get { return down_but.Visible; }
            set { down_but.Visible = value; }
        }

        private void down_but_Click(object sender, EventArgs e)
        {
            title_txt = "";
            main_txt = "Це був період нагромадження етнографічних знань, який можна вважати початком української етнографи (XV—ХVІ ст.). Другий стан - період наукового зібрання "
            +"та осмислення етнографічннх матеріалів. У 1772 р. з'явилася друком праця \"Описание свадебних украинских простонародных обычаев в М. России и в Украинской губ., "
            +"також и в великороссийских слободах, населенных малороссиянами, употребляемых\". Автором цієї унікальної пам'ятки української фольклористики був Григорій "
            +"Калиновський, офіцер російської армії, українець за походженням, його працю використовували, і в подальшому інші дослідники.\n\n"
            +"Становлення української етнографії як окремої наукової дисципліни відбулося у середині XIX ст. її розвиток тісно повязаний з іменами Михайла Максимовича, Миколи "
            +"Костомарова, Павла Чубинського. Миколи Сумцова, Володимира Шухевича, Федора Вовка, Дмитра Яворницького та багатьох інших видатних діячів української культури. "
            +"Значний внесок у розвиток українського народознавства зробили також письменники Тарас Шевченко, Пантелеймон Куліш, Іван Нечуй-Левицький, Іван Франко, Леся Українка, "
            +"Михайло Коцюбинський та ін. Провідну роль у дослідженнях з етнографії відіграло Львівське наукове товариство ім. Т.Г Шевченка, при якому в 1898 р. була створена "
            +"Етнографічна комісія, яка видавала \"етнографічний вісник\". - Матеріали до українсько-руської етнології\" тощо.\n\n"
            +"Сьогодні ж дослідженнями з етнографії займаються інститути Академії наук України, етноцентри, музеї, навчальні заклали, які видають наукові записки, монографії, "
            +"науково-популярні журнали та методичні посібники.";
            up_en = true;
            down_en = false;
        }

        private void up_but_Click(object sender, EventArgs e)
        {
            title_txt = "Етнографія";
            main_txt = "це історія народу, яка включає в себе історію його житла, одягу, харчування, його родинного укладу, форм побуту у широкому сенсі цього слова. "
            + "Це історія його світогляду, народних знань, вірувань і марновірства, обрядів і звичаїв. Останнім часом більшість учених відносять етнографію не до історичної, а до "
            + "суспільствознавчої науки.\n\n"
            + "Безпосередні спостереження і опис звичаїв, побуту, світогляду народу - це основний метод етнографії. Крім того, вона широко використовує писемні джерела, речові "
            + "пам'ятки, дані археології, антропології. Географії, фольклористки, мовознавства.\n\n"
            + "Перші описи давніх народів. їх характеристики наявні у Велесовій Книзі (про готів, греків, римлян та ін.). Спроби описати етнографічні особливості племен і народів "
            + "давньої України с також у \"Повісті времяних літ\". Літописець намагається накреслити генеалогію народів світу, іноді подаючи їхні культурно-побутові особливості, а "
            + "також описує староукраїнські племена Київської Русі, вказуючи місця їхнього розселення, відмінності у звичаях, обрядах тощо. Такі спроби етнографічних описів наявні "
            + "і у деяких інших писемних пам'ятках. Наприклад, у Галицько-Волинському літописі згадуються народи, які межують із Руссю: ляхи, литовці, угри, чехи, половці, жмудь, "
            + "яншян, татари, німці, ізраїльтяни. Тут же знаходимо відомості про звичаї кожного народу, описи костюмів (наприклад, костюм князя Володимира Васпльковнча).\n\n"
            + "До цінних етнографічних джерел можна віднести стародавні малюнки, ілюстрації до книжок, фрески, скульптурні чи рельєфні зображення людей, споруд, предметів побуту "
            + "тощо.\n\n"
            + "У хроніці Георгія Арматола, перекладеній київським книжником, вміщено 127 ілюстрацій, які відтворюють церковну архітектуру, одяг, обряди, народні вірування. описи "
            + "звичаїв та одягу часто знаходимо у фольклорі, особливо в козацьких думах згадаймо хоча б відому \"Думу про козака Голоту\"). Чимало цікавих відомостей про українців "
            + "затінили нам іночемш мандрівники, посіп, купці, які подовжувані Україною: Амвросій Контаріні, Гійом Л. де Боплан, Пєр Шевальє, Павло Алепський та ін.";
            up_en = false;
            down_en = true;
        }
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GIS
{
    public partial class Form33 : Form
    {
        public Form33()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form28 askania = new Form28();
            askania.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form29 aska = new Form29();
            aska.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form30 blacksea = new Form30();
            blacksea.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form31 karpat = new Form31();
            karpat.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form32 dunay = new Form32();
            dunay.ShowDialog();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Form34 bakay = new Form34();
            bakay.ShowDialog();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Form35 gayki = new Form35();
            gayki.ShowDialog();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Form36 yagor = new Form36();
            yagor.ShowDialog();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Form37 diril = new Form37();
            diril.ShowDialog();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Form38 sagi = new Form38();
            sagi.ShowDialog();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Form39 zolob = new Form39();
            zolob.ShowDialog();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Form40 ingulets = new Form40();
            ingulets.ShowDialog();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Form41 saga = new Form41();
            saga.ShowDialog();
        }

       
    }
}

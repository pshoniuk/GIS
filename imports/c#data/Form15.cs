﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GIS
{
    public partial class Form15 : Form
    {
        public Form15()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            n_climate prclim = new n_climate();
            prclim.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form16 park = new Form16();
            park.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form27 biosphere = new Form27();
            biosphere.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form33 zakaz = new Form33();
            zakaz.ShowDialog();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GIS
{
    public partial class parks : Form
    {
        public parks()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            resurce_std resource = new resurce_std();
            resource.Text = "Карпатський національний природний парк";
            resource.res_head = "Карпатський національний природний парк";
            resource.res_top = "національний парк в Україні, на території Івано-Франківської області. Створений для збереження унікальних лісових екосистем Центральної Європи.\n\n"
            +"Карпатський національний природний парк перший і один з найбільших в Україні національних природних парків. Він був створений постановою Ради "
            +"Міністрів УРСР від 3 червня 1980 р. № 376. Ще в 1921 році у межах його нинішньої території на площі 447 га було створено резерват для охорони Чорногірських пралісів. "
            +"Частина нинішньої території парку з 1968 по 1980 рік була у складі Карпатського державного заповідника, від якого при створенні парку були відокремлені Говерлянське "
            +"і Високогірне лісництва.";
            resource.res_bot = "";
            resource.BackgroundImage = Image.FromFile(@"src\009.jpg");
            resource.res_pos = 9;
            resource.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            resurce_std resource = new resurce_std();
            resource.Text = "Національний природний парк Шацький";
            resource.res_head = "Національний природний парк Шацький";
            resource.res_top = "національний парк в Україні, розташований у Шацькому районі Волинської області.\n\n"
            +"Створений 28 грудня 1983 року для охорони рідкісних природних комплексів у районі Шацьких озер.";
            resource.res_bot = "";
            resource.BackgroundImage = Image.FromFile(@"src\010.jpg");
            resource.res_pos = 10;
            resource.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            resurce_std resource = new resurce_std();
            resource.Text = "Національний природний парк “Нижньодністровський”";
            resource.res_head = "Національний природний парк “Нижньодністровський”";
            resource.res_top = "природоохоронна територія, національний природний парк в Україні. Розташований у "
            +"межах Білгород-Дністровського, Біляївського та Овідіопольського районів Одеської області.";
            resource.res_bot = "";
            resource.BackgroundImage = Image.FromFile(@"src\011.jpg");
            resource.res_pos = 11;
            resource.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            resurce_std resource = new resurce_std();
            resource.Text = "Національний природний парк “Святі гори”";
            resource.res_head = "Національний природний парк “Святі гори”";
            resource.res_top = "національний природний парк, розташований в північній частині Донецької області України, в Слов'янському (11 957 га), Краснолиманському (27 665 "
            +"га) і Артемівському районах. Створений 13 лютого 1997 року Указом Президента України № 135/97.";
            resource.res_bot = "";
            resource.BackgroundImage = Image.FromFile(@"src\012.jpg");
            resource.res_pos = 12;
            resource.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            resurce_std resource = new resurce_std();
            resource.Text = "Національний природний парк “Синевир”";
            resource.res_head = "Національний природний парк “Синевир”";
            resource.res_top = "національний парк, розташований в Міжгірському районі Закарпатської області України. Парк створено 5 січня 1989 року (в 1974 році було організовано "
            +"ландшафтний заказник загальнодержавного значення «Синевирське озеро»). Своєю назвою парк зобов'язаний розташованому на його території озера Синевир - найбільшому "
            +"озеру Закарпаття площею 7 га. Загальна площа парку - 40400 га.";
            resource.res_bot = "";
            resource.BackgroundImage = Image.FromFile(@"src\013.jpg");
            resource.res_pos = 13;
            resource.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            resurce_std resource = new resurce_std();
            resource.Text = "Національний природній парк “Голосіївський”";
            resource.res_head = "Національний природній парк “Голосіївський”";
            resource.res_top = "це єдиний у Європі національний природний парк, розташований на території мегаполісу. Парк загальною площею 4525,52 га повністю знаходиться в межах м. Києва.";
            resource.res_bot = "";
            resource.BackgroundImage = Image.FromFile(@"src\014.jpg");
            resource.res_pos = 14;
            resource.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            resurce_std resource = new resurce_std();
            resource.Text = "Національний природний парк “Гуцульщина”";
            resource.res_head = "Національний природний парк “Гуцульщина”";
            resource.res_top = "національний природний парк в Україні, в межах Косівського району Івано-Франківській області.\n\n"
            + "Національний парк був створений з метою збереження, відтворення та раціонального використання генетичних ресурсів рослинного і тваринного світу, унікальних "
            +"природних комплексів та етнокультурного середовища Покутсько-Буковинських Карпат.";
            resource.res_bot = "";
            resource.BackgroundImage = Image.FromFile(@"src\015.jpg");
            resource.res_pos = 15;
            resource.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            resurce_std resource = new resurce_std();
            resource.Text = "Національний природний парк “Сколівські Бескиди”";
            resource.res_head = "Національний природний парк “Сколівські Бескиди”";
            resource.res_top = "розташований в південно-західній частині Львівської області (в центральній частині оригінального в геоботанічному і ландшафтному відношенні "
            +"гірського масиву Східних Бескид, що тягнуться від Словацько-польського кордону до ріки Мізуньки, яка є межею між Бескидами і Горганами) на території трьох "
            +"адміністративних районів: Сколівського, Дрогобицького та Турківського.";
            resource.res_bot = "";
            resource.BackgroundImage = Image.FromFile(@"src\016.jpg");
            resource.res_pos = 16;
            resource.ShowDialog();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            resurce_std resource = new resurce_std();
            resource.Text = "Гетьманський національний природний парк";
            resource.res_head = "Гетьманський національний природний парк";
            resource.res_top = "національний природний парк в Україні, на території Великописарівського, Охтирського та Тростянецького районів Сумської області.\n\n"
            + "Парк створено з метою збереження, відтворення і раціонального використання типових та унікальних природних комплексів Лівобережного лісостепу, зокрема заплави "
            +"річки Ворскли, що мають важливе природоохоронне, наукове, історико-культурне, естетичне, рекреаційне та оздоровче значення.";
            resource.res_bot = "";
            resource.BackgroundImage = Image.FromFile(@"src\017.jpg");
            resource.res_pos = 17;
            resource.ShowDialog();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            resurce_std resource = new resurce_std();
            resource.Text = "Дермансько-Острозький національний природний парк";
            resource.res_head = "Дермансько-Острозький національний природний парк";
            resource.res_top = "природоохоронна територія, національний природний парк в Україні. Розташований на території Здолбунівського (частково) та Острозького районів "
            +"Рівненської області.\n\n"
            +"Парк розташований у найвужчій, східній, частині Малого Полісся — Острозькій долині. Охоплює територію заплави річки Збитинка, що протікає між мальовничими пагорбами "
            +"Мізоцького кряжу та Кременецькими горами.";
            resource.res_bot = "";
            resource.BackgroundImage = Image.FromFile(@"src\018.jpg");
            resource.res_pos = 18;
            resource.ShowDialog();
        }
    }
}

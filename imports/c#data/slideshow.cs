﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GIS
{
    public partial class slideshow : Form
    {
        public slideshow()
        {
            InitializeComponent();
        }

        public System.Drawing.Image img
        {
            get { return pictureBox1.Image; }
            set { pictureBox1.Image = value; }
        }

        public System.Windows.Forms.ListBox img_list
        {
            get { return imgs; }
            set { imgs = value; }
        }

        public System.Windows.Forms.ProgressBar count
        {
            get { return progressBar1; }
            set { progressBar1 = value; }
        }

        public Image playpause
        {
            get { return pictureBox2.Image; }
            set { pictureBox2.Image = value; }
        }

        public bool start
        {
            get { return timer1.Enabled; }
            set { timer1.Enabled = value; }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            int max = progressBar1.Maximum;
            int pos = progressBar1.Value;
            if (pos < max) pos++; else pos = 0;
            progressBar1.Value = pos;
            pictureBox1.Image = Image.FromFile(imgs.Items[pos].ToString());
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int max = progressBar1.Maximum;
            int pos = progressBar1.Value;
            if (pos < max) pos++; else pos = 0;
            progressBar1.Value = pos;
            pictureBox1.Image = Image.FromFile(imgs.Items[pos].ToString());
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (start == true) { start = false; playpause = Image.FromFile(@"src\play.png"); } else { start = true; playpause = Image.FromFile(@"src\pause.png"); }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GIS
{
    public partial class tourist : Form
    {
        public tourist()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            n_climate prclim = new n_climate();
            prclim.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            n_antropo antropogen = new n_antropo();
            antropogen.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            history istor = new history();
            istor.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            events podii = new events();
            podii.main_txt = "Подієві ресурси належать до динамічних чинників формування туристичних потоків, оскільки включають мотиваційні передумови подорожі до місця, де "
            +"відбувається подія чи явище. Спортивні змагання, культурно-мистецькі заходи, політичні акції, унікальні природні явища - далеко не повний перелік подій, які "
            +"спонукають людину до мандрівки. Подієві ресурси охоплюють найбільш суттєві прояви сучасного суспільного буття із його модою на глобалізацію, екологію, активний і "
            +"здоровий спосіб життя, переконаннями й уявленнями про стиль і стереотип поведінки людини у соціумі, включають його пропаганду та механізми реалізації. Необхідно "
            +"зазначити, що дана теза найбільшою мірою стосується частини світової спільноти, яку прийнято називати Західним світом.\n\n"
            +"Окремі види подієвих ресурсів найбільш доцільно класифікувати за тематикою заходу:\n\n"
            +"  - ГРОМАДСЬКІ\n\n"
            +"Гастрономічніфестивалі - пивний фестиваль \"Октоберфест\" (Мюнхен, Німеччина), свято молодого вина Божоле Нуво (Франція), фестиваль морепродуктів на о. Гров "
            +"(Іспанія), \"Свято меду\" (Мукачеве, Закарпаття).\n\n"
            +"Фестивалі і виставки квітів - виставка тюльпанів (Нідерланди), фестиваль квітів у Челсі (Велика Британія), свято троянд (травень) і жасмину (серпень) у Грассі "
            +"(Франція), фестиваль хризантем (Японія), фестиваль квітів (Таїланд), виставка квіткових експозицій на Співочому полі (Київ).\n\n"
            +"  - МИСТЕЦЬКІ\n\n"
            +"Музичні татеатральні фестивалі і конкурси - Музичний конкурс \"Євробачення\", фестиваль джазу в Монтре (Швейцарія), \"Слов\'янський базар\" (Білорусь), \"Нова "
            +"хвиля\" (Латвія), \"Таврійські ігри\" (Україна), Фестиваль художнього драматичного мистецтва (Франція).";
            podii.up_en = false;
            podii.down_en = true;
            podii.variant = 1;
            podii.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            social sekonom = new social();
            sekonom.ShowDialog();
        }
    }
}

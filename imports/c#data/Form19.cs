﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;

namespace GIS
{
    public partial class Form19 : Form
    {
        public Form19()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            slideshow picture = new slideshow();
            picture.Text = "Національний природний парк “Нижньодністровський”";
            picture.img = pictureBox1.Image;
            picture.ShowDialog();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            slideshow picture = new slideshow();
            picture.Text = "Національний природний парк “Нижньодністровський”";
            picture.img = pictureBox2.Image;
            picture.ShowDialog();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            maps maps = new maps();
            maps.Text = "Національний природний парк “Нижньодністровський”";
            maps.center = new GMap.NET.PointLatLng(46.391824, 30.209398);
            maps.zoom = 11; maps.pos = 11;

            GMapOverlay markersOverlay = new GMapOverlay("markers");
            GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(46.391824, 30.209398),
              GMarkerGoogleType.green);
            markersOverlay.Markers.Add(marker);
            maps.map.Overlays.Add(markersOverlay);

            maps.ShowDialog();
        }

    }
}

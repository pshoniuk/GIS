﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GIS
{
    public partial class doslid : Form
    {
        public doslid()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Історична довідка про ГІС";
            string path = Application.StartupPath + @"\src\html\b21.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\aspect.jpg");
            browse.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Область ГІС";
            string path = Application.StartupPath + @"\src\html\b22.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\aspect.jpg");
            browse.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Визначення ГІС";
            string path = Application.StartupPath + @"\src\html\b23.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\aspect.jpg");
            browse.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Апаратні засоби ГІС";
            string path = Application.StartupPath + @"\src\html\b24.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\aspect.jpg");
            browse.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Програмне забезпечення ГІС";
            string path = Application.StartupPath + @"\src\html\b25.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\aspect.jpg");
            browse.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Функцій ГІС";
            string path = Application.StartupPath + @"\src\html\b26.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\aspect.jpg");
            browse.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Аналіз в ГІС";
            string path = Application.StartupPath + @"\src\html\b27.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\aspect.jpg");
            browse.ShowDialog();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Принципи ГІС";
            string path = Application.StartupPath + @"\src\html\b28.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\aspect.jpg");
            browse.ShowDialog();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            data dani = new data();
            dani.ShowDialog();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            input output = new input();
            output.ShowDialog();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            webpost browse = new webpost();
            browse.Text = "Аналітичні можливості ГІС";
            string path = Application.StartupPath + @"\src\html\b2111.html";
            browse.browser.Navigate(path);
            browse.BackgroundImage = Image.FromFile(@"src\aspect.jpg");
            browse.ShowDialog();
        }
    }
}

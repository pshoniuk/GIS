﻿namespace GIS
{
    partial class resurce_std
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(resurce_std));
            this.head_label = new System.Windows.Forms.Label();
            this.top_label = new System.Windows.Forms.Label();
            this.bottom_label = new System.Windows.Forms.Label();
            this.control_panel = new System.Windows.Forms.Panel();
            this.pos = new System.Windows.Forms.ProgressBar();
            this.to_map = new System.Windows.Forms.PictureBox();
            this.to_slideshow = new System.Windows.Forms.PictureBox();
            this.bg_list = new System.Windows.Forms.ImageList(this.components);
            this.control_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.to_map)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.to_slideshow)).BeginInit();
            this.SuspendLayout();
            // 
            // head_label
            // 
            this.head_label.AutoSize = true;
            this.head_label.BackColor = System.Drawing.Color.Transparent;
            this.head_label.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.head_label.ForeColor = System.Drawing.Color.Blue;
            this.head_label.Location = new System.Drawing.Point(12, 63);
            this.head_label.Name = "head_label";
            this.head_label.Size = new System.Drawing.Size(47, 17);
            this.head_label.TabIndex = 0;
            this.head_label.Text = "none";
            // 
            // top_label
            // 
            this.top_label.BackColor = System.Drawing.Color.Transparent;
            this.top_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.top_label.ForeColor = System.Drawing.SystemColors.InfoText;
            this.top_label.Location = new System.Drawing.Point(12, 85);
            this.top_label.Name = "top_label";
            this.top_label.Size = new System.Drawing.Size(612, 344);
            this.top_label.TabIndex = 1;
            this.top_label.Text = "none";
            // 
            // bottom_label
            // 
            this.bottom_label.BackColor = System.Drawing.Color.Transparent;
            this.bottom_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bottom_label.ForeColor = System.Drawing.SystemColors.InfoText;
            this.bottom_label.Location = new System.Drawing.Point(12, 409);
            this.bottom_label.Name = "bottom_label";
            this.bottom_label.Size = new System.Drawing.Size(776, 182);
            this.bottom_label.TabIndex = 5;
            this.bottom_label.Text = "none";
            // 
            // control_panel
            // 
            this.control_panel.BackColor = System.Drawing.Color.Transparent;
            this.control_panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.control_panel.Controls.Add(this.pos);
            this.control_panel.Controls.Add(this.to_map);
            this.control_panel.Controls.Add(this.to_slideshow);
            this.control_panel.Location = new System.Drawing.Point(630, 85);
            this.control_panel.Name = "control_panel";
            this.control_panel.Size = new System.Drawing.Size(158, 301);
            this.control_panel.TabIndex = 6;
            // 
            // pos
            // 
            this.pos.Location = new System.Drawing.Point(-1, 139);
            this.pos.Maximum = 1000;
            this.pos.Name = "pos";
            this.pos.Size = new System.Drawing.Size(158, 20);
            this.pos.TabIndex = 7;
            this.pos.Visible = false;
            // 
            // to_map
            // 
            this.to_map.BackgroundImage = global::GIS.Properties.Resources.maps;
            this.to_map.Cursor = System.Windows.Forms.Cursors.Hand;
            this.to_map.Location = new System.Drawing.Point(15, 158);
            this.to_map.Name = "to_map";
            this.to_map.Size = new System.Drawing.Size(128, 128);
            this.to_map.TabIndex = 2;
            this.to_map.TabStop = false;
            this.to_map.Click += new System.EventHandler(this.to_map_Click);
            // 
            // to_slideshow
            // 
            this.to_slideshow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.to_slideshow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.to_slideshow.Image = global::GIS.Properties.Resources.slideshow;
            this.to_slideshow.Location = new System.Drawing.Point(15, 15);
            this.to_slideshow.Name = "to_slideshow";
            this.to_slideshow.Size = new System.Drawing.Size(128, 128);
            this.to_slideshow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.to_slideshow.TabIndex = 0;
            this.to_slideshow.TabStop = false;
            this.to_slideshow.Tag = "";
            this.to_slideshow.Click += new System.EventHandler(this.to_slideshow_Click);
            // 
            // bg_list
            // 
            this.bg_list.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("bg_list.ImageStream")));
            this.bg_list.TransparentColor = System.Drawing.Color.Transparent;
            this.bg_list.Images.SetKeyName(0, "geolog.jpg");
            // 
            // resurce_std
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.control_panel);
            this.Controls.Add(this.bottom_label);
            this.Controls.Add(this.top_label);
            this.Controls.Add(this.head_label);
            this.ForeColor = System.Drawing.SystemColors.Desktop;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "resurce_std";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "none";
            this.control_panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.to_map)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.to_slideshow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label head_label;
        private System.Windows.Forms.Label top_label;
        private System.Windows.Forms.Label bottom_label;
        private System.Windows.Forms.Panel control_panel;
        private System.Windows.Forms.PictureBox to_slideshow;
        private System.Windows.Forms.PictureBox to_map;
        private System.Windows.Forms.ProgressBar pos;
        private System.Windows.Forms.ImageList bg_list;
    }
}
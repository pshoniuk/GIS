﻿namespace GIS
{
    partial class transport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(transport));
            this.text = new System.Windows.Forms.Label();
            this.up_but = new System.Windows.Forms.PictureBox();
            this.down_but = new System.Windows.Forms.PictureBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.up_but)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.down_but)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // text
            // 
            this.text.BackColor = System.Drawing.Color.Transparent;
            this.text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.text.ForeColor = System.Drawing.SystemColors.InfoText;
            this.text.Location = new System.Drawing.Point(12, 62);
            this.text.Name = "text";
            this.text.Size = new System.Drawing.Size(776, 406);
            this.text.TabIndex = 1;
            this.text.Text = "none";
            // 
            // up_but
            // 
            this.up_but.BackColor = System.Drawing.Color.Transparent;
            this.up_but.Cursor = System.Windows.Forms.Cursors.Hand;
            this.up_but.Image = global::GIS.Properties.Resources.up_blue;
            this.up_but.Location = new System.Drawing.Point(710, 471);
            this.up_but.Name = "up_but";
            this.up_but.Size = new System.Drawing.Size(36, 26);
            this.up_but.TabIndex = 2;
            this.up_but.TabStop = false;
            this.up_but.Click += new System.EventHandler(this.up_but_Click);
            // 
            // down_but
            // 
            this.down_but.BackColor = System.Drawing.Color.Transparent;
            this.down_but.Cursor = System.Windows.Forms.Cursors.Hand;
            this.down_but.Image = global::GIS.Properties.Resources.down_blue;
            this.down_but.Location = new System.Drawing.Point(752, 471);
            this.down_but.Name = "down_but";
            this.down_but.Size = new System.Drawing.Size(36, 26);
            this.down_but.TabIndex = 3;
            this.down_but.TabStop = false;
            this.down_but.Click += new System.EventHandler(this.down_but_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(603, 20);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(149, 18);
            this.progressBar1.TabIndex = 4;
            this.progressBar1.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::GIS.Properties.Resources.trans_1;
            this.pictureBox1.Location = new System.Drawing.Point(12, 473);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(180, 115);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = global::GIS.Properties.Resources.trans_2;
            this.pictureBox2.Location = new System.Drawing.Point(198, 473);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(180, 115);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // transport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::GIS.Properties.Resources.transport1;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.down_but);
            this.Controls.Add(this.up_but);
            this.Controls.Add(this.text);
            this.ForeColor = System.Drawing.SystemColors.Desktop;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "transport";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Транспортна мережа";
            ((System.ComponentModel.ISupportInitialize)(this.up_but)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.down_but)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label text;
        private System.Windows.Forms.PictureBox up_but;
        private System.Windows.Forms.PictureBox down_but;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}
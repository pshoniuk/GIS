require("colors");
const fs = require("fs"),
	diff = require("diff"),
	path = require("path"),
	recursive = require("recursive-readdir"),
	fName = "textContent.tpl",
	regexps = [
		[/(см|м|км)\s*(2|3)\b/gim, "$1&sup$2"], //metrics
		[/\s*["`]\s*/gim, (findSymbol, pos, str, state) => {
			const symbol = findSymbol.trim();
			if (state.count == null) state.count = 0;
			state.count++;
			if (state.count % 2) return ` ${symbol}`;
			return `${symbol} `;
		}],
		[/,+/gim, ","],
		[/[ ]{2,}/gim, " "],
		[/(([^\x00-\x7F]|\w)+)\s+([,.!?;:])/gim, "$1$3"],
		[/([,:])(([^\x00-\x7F]|\w)+)/gim, "$1 $2"],
		[/(([^\x00-\x7F]|\w)+)[ ]+/gim, "$1 "],
		[/^[\s]+/gim, ""],
		[/(([^\s])+)[ ]+([.!?;%])/gim, "$1$3"],
		[/([.!?;])([^\x00-\x7F]|\w)/gim, "$1 $2"],
		[/([.!?;])[ ]+([^\x00-\x7F]|\w)/gim, "$1 $2"],
		[/"([а-яА-ЯёЁ].*?)"/gim, "«$1»"],
		[/«([0-9].*?)»/gim, "\"$1\""],
		[/(.+)\s+-\s+(.+)/gim, "$1 — $2"],
		[/(\()[ ]+(([^\x00-\x7F]|\w)+)/gim, "$1$2"],
		[/(([^\x00-\x7F]|\w)|\s+)[ ]+(\))/gim, "$1$3"],
		[/(\))(([^\x00-\x7F]|\w)+)/gim, "$1 $2"],
		[/(\))[ ]+(([^\x00-\x7F]|\w)+)/gim, "$1 $2"],
		[/(\d)\s*,\s*(\d)/gim, "$1,$2"] ,
		[/([^\x00-\x7F]|\w)\s*\.\s*(jpg|jpeg|png|bmp|gif|mp3|mp4)/gim, "$1.$2"]
	];


recursive("../app/resourses/", [(f, s) => !s.isDirectory() && path.basename(f) !== fName], (err, files) => {
	if(err) throw err;

	files.forEach((filePath) => {
		fs.readFile(path.resolve(filePath), "utf-8", (err, file) => {
			if(err) throw err;

			const newContent = regexps.reduce((content, regexp) => {
				let replacer = regexp[1];
				if(typeof replacer === "function"){
					const state = {};
					replacer = (...args) => {
						args.push(state);
						return regexp[1].apply(this, args);
					};
				}
				return content.replace(regexp[0], replacer);
			}, file || "");

			fs.writeFile(path.resolve(filePath), newContent, (err) => {
				if(err) throw err;
				if(newContent === file) return;

				console.info(filePath + " => Formatted");
				diff.diffChars(file, newContent).forEach((part) => {
					const color = part.added ? "green" : part.removed ? "red" : "grey";
					process.stderr.write(part.value[color]);
				});
				console.log();
			})
		});
	});
});
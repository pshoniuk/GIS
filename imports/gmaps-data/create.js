var fs = require("fs");
var readDir = {
	getListFiles: function(path){
		var list = [], e;
		if(!path) return list;
		try{
			return fs.readdirSync(path);
		}catch(e){
			return list;
		}
	},

	getFile: function(fileName){
		var file = "", e;
		if(!fileName) return ;
		try{
			file = fs.readFileSync(fileName, "utf8");
		}catch(e){}
		return file;
	},

	getFiles: function(path){
		var list = [], files = [], i, fileName, e;
		if(!path) return list;
		list = this.getListFiles(path);
		if(!list){
			return null;
		}
		for(i = 0; i < list.length; i++){
			fileName = list[i];
			try{
				files.push(fs.readFileSync(path + fileName, "utf8"));
			}catch(e){}
		}
		return files;
	}
};


var gDataRegExp = /maps\.Text\s+=\s+"+\\*"?(.*?)\\*"+[\s\S]*?maps\.center.*?Lng\(\s*(.+?),\s*(.+?)\)[\s\S]*?maps\.zoom.*?(\d+)[\s\S]*?marker\s*=\s*[\s\S]*?\([\s\S]*?\(\s*(.+?),\s*(.+?)\)/gmi;
var replaceChar = /["+]|(\\")/gm;
var file = readDir.getFile(process.cwd() + "\\resurce_std.cs");
var writeFile = fs.openSync(process.cwd() + "\\data.html", "w");
var blockCounter = 0;


var toUpperCase = function(str){
	str.replace(/[a-zа-я]/, function(c){
		return c.toUpperCase();
	});
	return str;
};


var json = require("../../app/js/source/data.js");
var lastCaption = ""
var aliases = {};


var recursive = function(obj){
	var key, val;
	if(obj && typeof obj === "object"){
		for(key in obj){
			val = obj[key];
			recursive(val);
			if(key === "caption"){
				lastCaption = val.trim().toLowerCase().replace(replaceChar, "");
			}else if(key === "route" && lastCaption){
				aliases[lastCaption] = val; 
			}
		}
	}
};

recursive(json);
console.log("ALL ALIASES: ", JSON.stringify(aliases, null, 4));
var counterDefinedGData = 0;


var matchBlock, chapterName, gData, alias;
while(matchBlock = gDataRegExp.exec(file)){
	gData = {
		caption: matchBlock[1],
		mapGeneral: {
			lat: +matchBlock[2],
			lng: +matchBlock[3],
			zoom: +matchBlock[4]
		},
		marker: {
			lat: +matchBlock[5],
			lng: +matchBlock[6]
		}
	};
	fs.writeSync(writeFile, "<pre>" + (blockCounter + 1) + ":<br>" + JSON.stringify(gData, null, 4) + "</pre><hr>");
	chapterName = matchBlock[1];
	alias = aliases[chapterName.trim().toLowerCase()];
	if(alias){
		+function(alias, chapterName, gData){
			fs.open("../../app/resourses/" + alias.replace(/#?content\//i, "") + "\\gmaps.dat", "w", function(err, file){
				if(!err){
					fs.write(file, JSON.stringify(gData, null, 4), function(){});
					console.log(++counterDefinedGData, chapterName, alias);
				}
			});
		}(alias, chapterName, gData);
		
	}
	blockCounter++;
}



console.log("\n\nTemplates" + blockCounter);
fs.closeSync(writeFile);